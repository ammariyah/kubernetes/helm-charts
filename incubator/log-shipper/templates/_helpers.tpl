{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "log-shipper.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "log-shipper.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "log-shipper.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Return the appropriate apiVersion for RBAC APIs.
*/}}
{{- define "rbac.apiVersion" -}}
{{- if semverCompare "^1.8-0" .Capabilities.KubeVersion.GitVersion -}}
"rbac.authorization.k8s.io/v1"
{{- else -}}
"rbac.authorization.k8s.io/v1beta1"
{{- end -}}
{{- end -}}

{{/*
Return the appropriate apiVersion for Daemonset APIs.
*/}}
{{- define "daemonset.apiVersion" -}}
{{- if semverCompare ">=1.16.0-0" .Capabilities.KubeVersion.GitVersion -}}
"apps/v1"
{{- else -}}
"extensions/v1beta1"
{{- end -}}
{{- end -}}

{{/*
Generating kafka broker in comma separated lists
*/}}
{{- define "log-shipper.kafkaBrokers" -}}
{{- $kb := dict "servers" (list) -}}
{{- if .Values.outputs.kafka.enabled -}}
{{- range $broker := .Values.outputs.kafka.brokers -}}
{{- $server := $broker.server -}}
{{- $port := toString $broker.port | default "9092" -}}
{{- $noop := printf "%s:%s" $server $port | append $kb.servers | set $kb "servers" -}}
{{- end -}}
{{- join "," $kb.servers -}}
{{- end -}}
{{- end -}}
